<?php
 
 class Home extends CI_Controller{

	public function index(){
		$this->load->helper('url');
		$this->load->view('components/header');
		$this->load->view('components/homeBanner');
		$this->load->view('components/sidebar');
		$this->load->view('home');
		$this->load->view('components/footer');

	}
 }

